package com.sg.service.sma;

import com.sg.service.SimpleMovingAvgService;
import com.sg.service.model.Stock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/** reference: https://nullbeans.com/how-to-calculate-the-relative-strength-index-rsi/ */
@Service
@Slf4j
public class SimpleMovingAvgServiceImpl implements SimpleMovingAvgService {


  public Mono<Double> calcSmaUp(Stock[] candlesticks, double totalNoOfDaysForCompute, int i, double avgUt1) {

    if (avgUt1 == 0) {
      double sumUpChanges = 0;

      for (int j = 0; j < totalNoOfDaysForCompute; j++) {
        double change = candlesticks[i - j].getClosePrice() - candlesticks[i - j].getOpenPrice();

        if (change > 0) {
          sumUpChanges += change;
        }
      }
      return Mono.just(sumUpChanges / totalNoOfDaysForCompute);
    } else {
      double change = candlesticks[i].getClosePrice() - candlesticks[i].getOpenPrice();
      if (change < 0) {
        change = 0;
      }
      return Mono.just(((avgUt1 * (totalNoOfDaysForCompute - 1)) + change) / totalNoOfDaysForCompute);
    }
  }

  public Mono<Double> calcSmaDown(Stock[] candlesticks, double totalNoOfDaysForCompute, int i, double avgDt1) {
    if (avgDt1 == 0) {
      double sumDownChanges = 0;

      for (int j = 0; j < totalNoOfDaysForCompute; j++) {
        double change = candlesticks[i - j].getClosePrice() - candlesticks[i - j].getOpenPrice();

        if (change < 0) {
          sumDownChanges -= change;
        }
      }
      return Mono.just(sumDownChanges / totalNoOfDaysForCompute);
    } else {
      double change = candlesticks[i].getClosePrice() - candlesticks[i].getOpenPrice();
      if (change > 0) {
        change = 0;
      }
      return Mono.just(((avgDt1 * (totalNoOfDaysForCompute - 1)) - change) / totalNoOfDaysForCompute);
    }
  }
}
