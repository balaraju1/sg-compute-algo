package com.sg.service.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;

@NoArgsConstructor
@Data
public class Stock {
  private Long openTime;
  private double openPrice;
  private double closePrice;
}
