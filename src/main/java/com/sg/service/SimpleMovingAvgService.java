package com.sg.service;

import com.sg.service.model.Stock;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public interface SimpleMovingAvgService {

  public Mono<Double> calcSmaUp(Stock[] candlesticks, double totalNoOfDaysForCompute, int i, double avgUt1);

  public Mono<Double> calcSmaDown(Stock[] candlesticks, double totalNoOfDaysForCompute, int i, double avgDt1);

}
