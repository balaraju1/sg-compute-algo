package com.sg.service.rsi;

import com.sg.service.RSIService;
import com.sg.service.SimpleMovingAvgService;
import com.sg.service.model.Stock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/** reference: https://nullbeans.com/how-to-calculate-the-relative-strength-index-rsi/ */
@Slf4j
@Service
public class RSIServiceImpl implements RSIService {

  @Autowired private SimpleMovingAvgService simpleMovingAvgService;

  @Override
  public double[] calculateRSIValues(Stock[] candlesticks, int n) {

    double[] results = new double[candlesticks.length];

    Mono<Double> ut1 = Mono.empty();
    Mono<Double> dt1 = Mono.empty();
    for (int i = 0; i < candlesticks.length; i++) {
      if (i < (n)) {
        continue;
      }

      ut1 = simpleMovingAvgService.calcSmaUp(candlesticks, n, i, ut1.block());
      dt1 = simpleMovingAvgService.calcSmaDown(candlesticks, n, i, dt1.block());

      results[i] = 100.0 - 100.0 / (1.0 + (ut1.block() / dt1.block()));
    }

    return results;
  }
}
