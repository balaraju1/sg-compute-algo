package com.sg.service;

import com.sg.service.model.Stock;
import org.springframework.stereotype.Service;

@Service
public interface RSIService {
    
    public double[] calculateRSIValues(Stock[] candlesticks, int n);
}