package com.sg;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class SGRobotApplication {

  public static void main(String[] args) {
    SpringApplication.run(SGRobotApplication.class, args);
    System.out.println("inside demo app");
    log.info("inside log4j");
  }
}
